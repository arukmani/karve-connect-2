import { environment } from '@kozakatak/environment-loader';
import { join } from 'path';

export const cfg = environment({
    production: false,
    address: '127.0.0.1',
    port: 5000,

    title: 'KarveConnect',

    database: {
        initial_file: join(__dirname, '../data/initial_karveconnect.db'),
        filename: join(__dirname, '../data/karveconnect.db'),
    }
}, {});
